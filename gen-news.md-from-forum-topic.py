#!/usr/bin/env python3

import datetime
import feedparser
import gitlab
import json
import os
import re
import requests

headers = {'User-Agent': 'F-Droid TWIFbot'}

tag_re = re.compile(r'(<!--.*?-->|<[^>]*>)')

url = 'https://forum.f-droid.org/t/twif-submission-thread/2752.json?print=true'

r = requests.get(url, headers=headers)
r.raise_for_status()
data = r.json()
posts = data['post_stream'].get('posts')

feed = feedparser.parse('https://f-droid.org/feed.xml')
last = datetime.datetime.fromisoformat('2019-07-04')  # failsafe
for post in feed.entries:
    if '/twif' in post.id:
        last = datetime.datetime.fromisoformat(post['published']).replace(tzinfo=None)
        print('last TWIF:', post['published'])
        break

news = ''
for post in reversed(posts):
    created_at = datetime.datetime.strptime(post['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
    if created_at > last:
        news = tag_re.sub('', post['cooked']) + '\n' + news
    else:
        break

with open('untracked/news.md', 'w') as fp:
    fp.write('Starting from https://forum.f-droid.org/t/twif-submission-thread/2752/')
    fp.write(str(post['post_number']))
    fp.write('\n---\n')
    fp.write(news)

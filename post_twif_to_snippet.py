#!/usr/bin/env python3

import gitlab
import os
import sys

PERSONAL_ACCESS_TOKEN = os.getenv('PERSONAL_ACCESS_TOKEN')
gl = gitlab.Gitlab('https://gitlab.com', api_version=4,
                   private_token=PERSONAL_ACCESS_TOKEN)
snippet = gl.snippets.get(1889172)
project = gl.projects.get(snippet.project_id, lazy=True)
editable_snippet = project.snippets.get(snippet.id)
with open(sys.argv[1]) as fp:
    editable_snippet.code = fp.read()
editable_snippet.save()
